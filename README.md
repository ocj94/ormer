# Ormer

Ormer is a board game inspired by [Abalone](https://en.wikipedia.org/wiki/Abalone_(board_game)).

There is no game logic here, to reproduce the tangible game sensation: you can do whatever you want, follow the Abalone rules or yours.

Enjoy!

# dev

- [video devlogs](https://peertube.mastodon.host/video-channels/ormer_game/videos)
- [issues](https://gitlab.com/polymorphcool/ormer/issues)

# resources

- [abalone official rules](https://cdn.1j1ju.com/medias/c2/b0/3a-abalone-rulebook.pdf)
- [abalone alternative rules](https://dicelandblog.pl/wp-content/uploads/2017/05/abalone_setups.pdf)

# credits

- made with [godot](http://godotengine.org)
- font [Générale Mono](https://fontlibrary.org/en/font/generalemono) under [OFL (SIL Open Font License)](https://scripts.sil.org/cms/scripts/page.php?site_id=nrsi&id=OFL) by [Ariel Martín Pérez](https://fontlibrary.org/en/member/arielmartinperez)

