extends Node

var active_grid = null
var active_selection = null

func _ready():
#	var g = generate_grid( 6 )
#	save_grid( 'user://omer_grid_' + str( g.depth ) + '.json', g )
#	load_grid( 'user://omer_grid_6.json', true )
	pass

###################
# DATA STRUCTURES #
###################

# selection are preconfigured to match std abalone rules
func new_selection():
	return {
		'empty': true,
		'selected': [], 		# selected cell ids
		'selectable': [], 		# selectable cell ids, without selected
		'inline': true, 		# set to true to limit selection to lines
		'continuous': true,		# prevents holes in line selection
		'direction': -1, 		# used if inline is true
		'max': 3, 				# set to > 0 to limit selected cells
		'player': -1 			# player id
	}

func new_cell( lvl, corner, pos ):
	return { 
		'level': lvl, 
		'corner': corner, 
		'pos': pos, 
		'id': -1, 
		'player': -1
	}

func new_grid():
	return {
		'depth': -1,
		'cell_num': 0,
		'levels': [], # hierarchical cells 
		'cells': [] # fast cell access
	}

#############
# SELECTION #
#############

func selection_add( grid, selection, id ):
	
	var c = get_cell_by_id( grid, id )
	if c.player != selection.player:
		if OS.is_debug_build():
			printerr("cell_select::selection player != cell player: " + str( selection.player ) + ' <> ' + str( c.player ))
		return
	
	if selection.max != -1 and len( selection.selected ) >= selection.max:
		return
	
	if len( selection.selectable ) == 0 or id in selection.selectable:
		selection.selected.append( id )
	
	if len( selection.selected ) == 2 and selection.inline:
		for i in range( 0,6 ):
			if selection.selectable[i] == id:
				selection.direction = i % 3
				print( "selection direction: " + str( selection.direction ) )
				break
	
	selection_update( grid, selection )

func selection_remove( grid, selection, id ):
	
	if id in selection.selected:
		var prev = selection.selected
		selection.selected = []
		for ci in prev:
			if ci != id:
				selection.selected.append( ci )
	
	selection_update( grid, selection )

func selection_update( grid, selection ):
	
	selection.empty = len( selection.selected ) == 0
	
	if not selection.empty and selection.continuous:
		var valid = [selection.selected[0]]
		var vselectable = get_cells_around( grid, selection.selected[0] )
		for id in selection.selected:
			if id in vselectable:
				valid.append( id )
				vselectable += get_cells_around( grid, id )
		selection.selected = valid
	
	if len( selection.selected ) < 2:
		selection.direction = -1
	
	selection.selectable = []
	for ci in selection.selected:
		var around = get_cells_around( grid, ci )
		for i in range( 0, 6 ):
			var id = around[i]
			var c = get_cell_by_id( grid, id )
			if selection.direction != -1 and i%3 != selection.direction:
				continue
			if c.player != selection.player:
				if len( selection.selected ) == 1:
					selection.selectable.append( null )
				continue
			if id == ci:
				continue
			selection.selectable.append( id )

################
# MANIPULATION #
################

func validate_grid( grid ):
	if not 'depth' in grid:
		if OS.is_debug_build():
			printerr("validate_grid::missing 'depth' key")
		return false
	if not 'cell_num' in grid:
		if OS.is_debug_build():
			printerr("validate_grid::missing 'cell_num' key")
		return false
	if not 'levels' in grid or not grid.levels is Array:
		if OS.is_debug_build():
			printerr("validate_grid::missing 'levels' key")
		return false
	if not 'cells' in grid or not grid.cells is Array:
		if OS.is_debug_build():
			printerr("validate_grid::missing 'cells' key")
		return false
	for c in grid.cells:
		if not c is Array or len( c ) < 3:
			if OS.is_debug_build():
				printerr("validate_grid::grid.cells must be arrays of len 3")
			return false
		# all ids to int!
		for i in range(0,len( c )):
			c[i] = int(c[i])
		if get_cell_by_pos( grid, c[0], c[1], c[2] ) == null:
			if OS.is_debug_build():
				printerr("validate_grid::cell ", c, " is not valid")
			return false
	return true
	
func get_cell_by_id( grid, id ):
	if id == null or id < 0 or id >= grid.cell_num:
		if OS.is_debug_build():
			printerr("get_cell_by_id::id must be in range [0, ", (grid.cell_num-1), ']')
		return null
	var cd = grid.cells[ id ]
	return grid.levels[cd[0]][cd[1]][cd[2]]

func get_cell_by_pos( grid, level, corner, pos ):
	if level < 0 or level >= len(grid.levels):
		if OS.is_debug_build():
			printerr("get_cell_by_pos::level must be in range [0, ", (len(grid.levels)-1), ']')
		return null
	var l = grid.levels[ level ]
	if corner < 0 or corner >= len(l):
		if OS.is_debug_build():
			printerr("get_cell_by_pos::corner must be in range [0, ", (len(l)-1), ']')
		return null
	var c = l[corner]
	if pos < 0 or pos >= len(c):
		if OS.is_debug_build():
			printerr("get_cell_by_pos::pos must be in range [0, ", (len(c)-1), ']')
		return null
	return c[pos]

func get_cells_around( grid, id ):
	if id < 0 or id >= grid.cell_num:
		if OS.is_debug_build():
			printerr("get_cells_around::id must be in range [0, ", (grid.cell_num-1), ']')
		return null
	var cell = grid.cells[id]
	var arr = null
	if cell[0] == 0:
		arr = []
		for c in grid.levels[1]:
			arr.append( c[0] )
	elif cell[2] == 0: # cell in a corner
		arr = []
		if cell[0] > 1: # higher than level 1
			arr.append( get_cell_by_pos( grid, cell[0]+1, 	cell[1], 			cell[2] ) )
			arr.append( get_cell_by_pos( grid, cell[0]+1, 	cell[1], 			cell[2]+1 ) )
			arr.append( get_cell_by_pos( grid, cell[0], 	cell[1], 			cell[2]+1 ) )
			arr.append( get_cell_by_pos( grid, cell[0]-1, 	cell[1], 			cell[2] ) )
			arr.append( get_cell_by_pos( grid, cell[0], 	(cell[1]+5) % 6, 	cell[2]+(cell[0]-1) ) )
			arr.append( get_cell_by_pos( grid, cell[0]+1, 	(cell[1]+5) % 6, 	cell[2]+cell[0] ) )
		else: # level 1
			arr.append( get_cell_by_pos( grid, cell[0]+1, 	cell[1], 			cell[2] ) )
			arr.append( get_cell_by_pos( grid, cell[0]+1, 	cell[1], 			cell[2]+1 ) )
			arr.append( get_cell_by_pos( grid, cell[0], 	(cell[1]+1) % 6, 	cell[2] ) )
			arr.append( get_cell_by_pos( grid, 0, 			0, 					0 ) ) # center
			arr.append( get_cell_by_pos( grid, cell[0], 	(cell[1]+5) % 6, 	cell[2] ) )
			arr.append( get_cell_by_pos( grid, cell[0]+1, 	(cell[1]+5) % 6, 	cell[2]+1 ) )
	else:
		arr = []
		if cell[2] == cell[0]-1: # end of line
			arr.append( get_cell_by_pos( grid, cell[0]+1, 	cell[1], 			cell[2] ) )
			arr.append( get_cell_by_pos( grid, cell[0]+1, 	cell[1], 			cell[2]+1 ) )
			arr.append( get_cell_by_pos( grid, cell[0], 	(cell[1]+1) % 6, 	0 ) )
			arr.append( get_cell_by_pos( grid, cell[0]-1, 	(cell[1]+1) % 6, 	0 ) )
			arr.append( get_cell_by_pos( grid, cell[0]-1, 	cell[1], 			cell[2]-1 ) )
			arr.append( get_cell_by_pos( grid, cell[0], 	cell[1], 			cell[2]-1 ) )
		else: # next pos
			arr.append( get_cell_by_pos( grid, cell[0]+1, 	cell[1], 			cell[2] ) )
			arr.append( get_cell_by_pos( grid, cell[0]+1, 	cell[1], 			cell[2]+1 ) )
			arr.append( get_cell_by_pos( grid, cell[0], 	cell[1], 			cell[2]+1 ) )
			arr.append( get_cell_by_pos( grid, cell[0]-1, 	cell[1], 			cell[2] ) )
			arr.append( get_cell_by_pos( grid, cell[0]-1, 	cell[1], 			cell[2]-1 ) )
			arr.append( get_cell_by_pos( grid, cell[0], 	cell[1], 			cell[2]-1 ) )
	# sorting output to have constant directions, based on corner
	var cells = []
	var offset = 6-cell[1]
	for i in range( 0, 6 ):
		var c = arr[(i+offset) % 6]
		if c == null:
			cells.append( null )
		else:
			cells.append( c.id )
	return cells

func clone_cell( cell ):
	return {
		'level': cell.level, 
		'corner': cell.corner, 
		'pos': cell.pos, 
		'id': cell.id, 
		'player': cell.player
	}

func clone_grid( grid ):
	
	var out = new_grid()
	out.depth = grid.depth
	out.cell_num = grid.cell_num
	out.cells = grid.cells.duplicate(true)
	for lvl in grid.levels:
		var newl = []
		for corner in lvl:
			var newc = []
			for cell in corner:
				newc.append( clone_cell( cell ) )
			newl.append( newc )
		out.levels.append( newl )
	return out

func get_cells_count( grid, player ):
	
	var count = 0
	for c in grid.cells:
		if get_cell_by_pos( grid, c[0], c[1], c[2] ).player == player:
			count += 1
	return count

func reset_cells( grid, player ):
	for cd in grid.cells:
		var c = get_cell_by_pos( grid, cd[0], cd[1], cd[2] )
		if c.player == player:
			c.player = -1

func transfer_player( src_grid, dst_grid ):
	
	# smallest grid:
	var cnum = src_grid.cell_num
	if dst_grid.cell_num < cnum:
		cnum = dst_grid.cell_num
	
	# copying players
	for i in range( 0, cnum ):
		get_cell_by_id( dst_grid, i ).player = get_cell_by_id( src_grid, i ).player

##############
# GENERATION #
##############

func generate_grid( depth, dump = false ):
	
	var g = new_grid()
	g.depth = depth
	
	for lvl in range( 0, depth + 1 ):
		
		g.levels.append([])
		
		if lvl == 0:
			g.levels[0].append( [ new_cell( 0,0,0 ) ] )
			continue
		
		for corner in range( 0, 6 ):
			g.levels[lvl].append( [] )
			for pos in range(0, lvl):
				g.levels[lvl][corner].append( new_cell( lvl,corner,pos ) )
	
	var cid = 0
	for lvl in g.levels:
		for cs in lvl:
			for c in cs:
				c.id = cid
				g.cells.append( [ c.level,c.corner,c.pos ] )
				cid += 1
	g.cell_num = cid
	
	if dump:
		dump_grid( g )
	
	return g

func dump_grid( g ):
	print( '***grid***' )
	print( '\t', g.depth )
	print( '\t', g.cell_num )
	print( '\tlevels' )
	for cs in g.levels:
		print( '\t\t', cs )
	print( '\tcells' )
	for c in g.cells:
		print( '\t\t', c )

##############
# FILE MGMNT #
##############

func save( path, grid ):
	
	# removal of all additional data
	var g = clone_grid(grid)
	var file = File.new()
	file.open( path, File.WRITE )
	file.store_string(to_json(g))
	file.close()

func load( path, dump = false ):
	var file = File.new()
	if file.file_exists(path):
		file.open(path, File.READ)
		var g = parse_json(file.get_as_text())
		file.close()
		if typeof(g) == TYPE_DICTIONARY and validate_grid( g ):
			if dump:
				dump_grid( g )
			return g
		else:
			printerr("Corrupted data!")
	else:
		printerr("No saved data!")
	return null
