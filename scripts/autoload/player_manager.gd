extends Node

# warning-ignore:unused_class_variable
onready var pastille_material = load( "res://materials/circles.material" )
# warning-ignore:unused_class_variable
onready var decoy_material = load( "res://materials/decoy_ball.material" )
# warning-ignore:unused_class_variable
onready var ball_mat = load( "res://materials/ball.material" )

var players = []
var current_player = -1

func _ready():
	pass # Replace with function body.

###################
# DATA STRUCTURES #
###################

func new_player( color ):
	var p = {
		'color': color,
		'txt_color': color,
		'mat_pastille': pastille_material.duplicate(),
		'mat_decoy': decoy_material.duplicate(),
		'mat_ball': ball_mat.duplicate()
	}
	p.mat_decoy.next_pass = p.mat_decoy.next_pass.duplicate()
	if color == null:
		color = Color(1,1,1,1)
	set_color( p, color )
	return p

################
# MANIPULATION #
################

func add_player( color = null ):
	if color is Array:
		color = Color( color[0], color[1], color[2], color[3] )
	players.append( new_player( color ) )

func get_player( pid, default = 0 ):
	if pid == null:
		pid = -1
	if pid < 0 or pid >= len( players ):
		if default >= 0 and default < len( players ):
			return players[default]
		return null
	return players[pid]

func set_color( player, color ):
	
	if player == null:
		if OS.is_debug_build():
			printerr("set_color::no player dict given" )
		return
	if color == null or not color is Color:
		if OS.is_debug_build():
			printerr("set_color::no color given, ", color, ' type: ', typeof(color) )
		return
	
	player.color = color
	if player.color.gray() > 0.5:
		player.txt_color = Color(0,0,0,1)
	else:
		player.txt_color = Color(1,1,1,1)
	player.mat_pastille.set_shader_param( 'albedo', color )
	player.mat_decoy.set_shader_param( 'albedo', color )
	# invert color
	player.mat_decoy.next_pass.set_shader_param( 'albedo', Color(1-color.r,1-color.g,1-color.b,color.a))
	player.mat_ball.set_shader_param( 'albedo', color )

##############
# FILE MGMNT #
##############

func save( path ):

	# removal of all additional data
	var ps = []
	for p in players:
		ps.append( {
			'color': [p.color.r,p.color.g,p.color.b,p.color.a]
		} )
	var file = File.new()
	file.open( path, File.WRITE )
	file.store_string(to_json(ps))
	file.close()

func load( path ):
	var file = File.new()
	if file.file_exists(path):
		file.open(path, File.READ)
		var ps = parse_json(file.get_as_text())
		file.close()
		if typeof(ps) == TYPE_ARRAY:
			for p in ps:
				add_player( p.color )
			return true
		else:
			printerr("Corrupted data!")
	else:
		printerr("No saved data!")
	return false