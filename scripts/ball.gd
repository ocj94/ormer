extends RigidBody

onready var mesh_mat = null

func set_mat( mat ):
	print( mat )
	mesh_mat = mat
	$mesh.material_override = mat
	var c = get_albedo()
	$highlight.material_override = $highlight.material_override.duplicate()
	$highlight.material_override.albedo_color = Color( 1-c.r, 1-c.g, 1-c.b, c.a )

func get_mat():
	return mesh_mat

func get_mesh():
	return $mesh

func set_time_offset( f ):
	var m = $mesh.material_override.duplicate()
	m.set_shader_param( "time_offset", f )
	$mesh.material_override = m

func get_time_offset():
	return $mesh.material_override.get_shader_param( "time_offset" )

func get_albedo():
	return $mesh.material_override.get_shader_param( "albedo" )

func highlight( b ):
	$highlight.visible = b

func _ready():
	pass
