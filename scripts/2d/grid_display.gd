# grid data structure is defined in res://scripts/autoload/grid_manager.gd!

extends Node2D

onready var gm = get_node( "/root/grid_manager" )
onready var pm = get_node("/root/player_manager")

export (float,0,100) var radius = 50
export (float,0,100) var gap = 5

var hsprite = null
var buttons = null
var selectable_style_bg = Color( 0,1,90.0/255,1.0 )
var normal_style_bg = Color( 0.5,0.5,0.5,1.0 )

# TMP
var last_id = null

func _ready():
	
	# hexagon button template
	hsprite = load( "res://models/2d/hexa_button.tscn" ).instance()
	hsprite.radius = radius * 0.5
	hsprite.horizontal = true
	hsprite.selectable = false
	hsprite._text = ""
	hsprite.normal_style.bg = normal_style_bg
	hsprite.normal_style.border = Color( 0.5,0.5,0.5,0.0 )
	hsprite.normal_style.scale = Vector2(1.0,1.0)
	hsprite.hover_style.scale = Vector2(0.75,0.75)
	hsprite.hover_style.bg = Color( 0.5,0,45.0/255,1 )
	hsprite.selected_style.bg = Color( 1,0,90.0/255,1 )
	hsprite.selected_style.border = Color( 1,0,90.0/255,1 )
	hsprite.disabled_style.bg = Color( 0.5,0.5,0.5,0.0 )
	hsprite.get_node('txt').theme = Theme.new()
	hsprite.generate(true)

func _process(delta):
	pass

func select_around( id ):
	
	if gm.active_grid == null:
		return
	
	if last_id == id:
		last_id = null
		for ci in range(0,len(get_children())):
			get_child(ci).disable(false)
			get_child(ci).normal_style.bg = normal_style_bg
			get_child(ci).selected = false
		return
	
	var cells = gm.get_cells_around( gm.active_grid, id )
	if cells == null:
		return
	
	var i = 0
	var selectable = []
	selectable.append( id )
	for cid in cells:
		if cid != null:
			get_child(cid).normal_style.bg = Color.from_hsv( 0.0 + i / 24.0, 1.0, 0.75, 1.0)
			selectable.append( cid )
		i += 1
	
	# all buttons to normal
	for ci in range(0,len(get_children())):
		var cell = gm.get_cell_by_id( gm.active_grid, ci )
		if not ci in selectable:
			reset_button_style( ci )
			get_child(ci).disable(true)
			print( ci )
		else:
			get_child(ci).disable(false)
			get_child(ci).selected = ci == id
	
	last_id = id

func reset_button_style( id ):
	var btn = get_child( id )
	var cell = gm.get_cell_by_id( gm.active_grid, id )
	if cell.player == -1:
		btn.normal_style.bg = hsprite.normal_style.bg
		btn.normal_style.txt = hsprite.normal_style.txt
	else:
		var p = pm.get_player( cell.player )
		btn.normal_style.bg = p.color
		btn.normal_style.txt = p.txt_color

func render_selection():
	if gm.active_selection == null or gm.active_selection.empty:
		for ci in range(0,len(get_children())):
			var cell = gm.get_cell_by_id( gm.active_grid, ci )
			get_child(ci).disable(false)
			get_child(ci).selected = false
			reset_button_style( ci )
	else:
		for ci in range(0,len(get_children())):
			if ci in gm.active_selection.selected:
				get_child(ci).disable(false)
				get_child(ci).selected = true
			elif ci in gm.active_selection.selectable:
				get_child(ci).disable(false)
				get_child(ci).selected = false
				get_child(ci).normal_style.bg = selectable_style_bg
			else:
				get_child(ci).disable(true)
				get_child(ci).selected = false
				reset_button_style( ci )

func hexa_pressed( btn ):
	var cid = buttons[btn]
	if not cid in gm.active_selection.selected:
		if gm.active_selection.empty:
			var cell = gm.get_cell_by_id( gm.active_grid, cid )
			gm.active_selection.player = cell.player
		gm.selection_add( gm.active_grid, gm.active_selection, cid )
	else:
		gm.selection_remove( gm.active_grid, gm.active_selection, cid )
	render_selection()

func add_hexa( tmpl, cell_data, cell_id ):
	var btn = tmpl.duplicate()
	btn._text = str(cell_data[0]) + ":" + str(cell_data[1]) + ":" + str(cell_data[2])
	btn.hexa_connect( self, "hexa_pressed" )
	var cell = gm.get_cell_by_id( gm.active_grid, cell_id )
	if cell.player != -1:
		var p = pm.get_player( cell.player )
		btn.normal_style.bg = p.color
		btn.normal_style.txt = p.txt_color
	buttons[btn] = cell_id
	add_child(btn)
	return btn

func synchronise():
	
	if gm.active_grid == null:
		return
	var grid = gm.active_grid
	
	while get_child_count() > 0:
		remove_child( get_child(0) ) 
	buttons = {}
	
	var ci = 0
	var spi6 = sin( PI / 6 )
	
	for li in len(grid.levels):
		var lvl = grid.levels[li]
		if li == 0:
			var h = add_hexa( hsprite, grid.cells[ci], ci )
			h.position = Vector2(0,0)
			ci += 1
		else:
			var rad = li * ( radius * 2 + gap ) * spi6
			var a = PI / 6
			for corner in lvl:
				var cp = Vector2( cos( a ), sin( a ) ) * rad
				var a60 = a + TAU / 3
				var off =  Vector2( cos( a60 ), sin( a60 ) ) * ( radius * 2 + gap ) * spi6
				for i in li:
					var h = add_hexa( hsprite, grid.cells[ci], ci )
					h.position = cp + off * i
					ci += 1
				a += PI / 3
